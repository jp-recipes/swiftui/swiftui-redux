//
//  AddTaskView.swift
//  swiftui-redux-hello
//
//  Created by JP on 23-08-23.
//

import SwiftUI

struct AddTaskView: View {
    @EnvironmentObject var store: Store<AppState>

    @State private var name = ""

    struct Props {
        let tasks: [Task]
        let onTaskAdded: (Task) -> Void
    }

    private func map(state: TaskState) -> Props {
        return Props(tasks: state.tasks) { task in
            store.dispatch(action: AddTaskAction(task: task))
        }
    }

    var body: some View {
        let props = map(state: store.state.taskState)

        VStack {
            Form {
                Section("Add Task") {
                    TextField("Enter task", text: $name)

                    Button("Add") {
                        let task = Task(title: name)
                        props.onTaskAdded(task)
                    }
                }

                Section("Task saved") {
                    List(props.tasks) { task in
                        Text(task.title)
                    }
                }
            }


        }
    }
}

struct AddTaskView_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store(reducer: appReducer, state: AppState())

        AddTaskView().environmentObject(store)
    }
}
