//
//  swiftui_redux_helloApp.swift
//  swiftui-redux-hello
//
//  Created by JP on 23-08-23.
//

import SwiftUI

@main
struct swiftui_redux_helloApp: App {
    var body: some Scene {
        let store = Store(reducer: appReducer, state: AppState(), middlewares: [
            logMiddleware(),
            incrementMiddleware()
        ])

        return WindowGroup {
            CounterView().environmentObject(store)
        }
    }
}
