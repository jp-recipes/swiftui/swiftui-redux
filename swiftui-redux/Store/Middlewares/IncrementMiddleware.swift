//
//  IncrementMiddleware.swift
//  swiftui-redux-hello
//
//  Created by JP on 24-08-23.
//

import Foundation

func incrementMiddleware() -> Middleware<AppState> {
    return { state, action, dispatch in
        switch action {
        case _ as IncrementActionAsync:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                dispatch(IncrementAction())
            }
        default:
            break
        }
    }
}
