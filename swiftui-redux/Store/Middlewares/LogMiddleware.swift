//
//  LogMiddleware.swift
//  swiftui-redux-hello
//
//  Created by JP on 24-08-23.
//

import Foundation

func logMiddleware() -> Middleware<AppState> {
    return { state, action, dispatch in
        print("[LOG MIDDLEWARE]")
    }
}
