//
//  ContentView.swift
//  swiftui-redux-hello
//
//  Created by JP on 23-08-23.
//

import SwiftUI

struct CounterView: View {
    @State private var isPresented = false

    @EnvironmentObject var store: Store<AppState>

    struct Props {
        let counter: Int
        let onIncrement: () -> Void
        let onDecrement: () -> Void
        let onAdd: (Int) -> Void
        let onIncrementAsync: () -> Void
    }

    private func map(state: CounterState) -> Props {
        Props(counter: state.counter) {
            store.dispatch(action: IncrementAction())
        } onDecrement: {
            store.dispatch(action: DecrementAction())
        } onAdd: { value in
            store.dispatch(action: AddAction(value: value))
        } onIncrementAsync: {
            store.dispatch(action: IncrementActionAsync())
        }
    }

    var body: some View {
        let props = map(state: store.state.counterState)

        VStack(spacing: 20) {
            Text("\(props.counter)")
                .padding()
            Button("Increment") {
                props.onIncrement()
            }
            Button("Decrement") {
                props.onDecrement()
            }
            Button("Add") {
                props.onAdd(100)
            }
            Button("Increment Async") {
                props.onIncrementAsync()
            }

            Spacer()

            Button("Add Task") { isPresented = true }

            Spacer()
        }
        .sheet(isPresented: $isPresented) {
            AddTaskView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store(reducer: appReducer, state: AppState())

        return CounterView().environmentObject(store)
    }
}
