//
//  Task.swift
//  swiftui-redux-hello
//
//  Created by JP on 23-08-23.
//

import Foundation

struct Task: Identifiable {
    let id = UUID()
    let title: String
}
